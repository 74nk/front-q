(function() {
    'use strict';

    var input = document.getElementById('input');
    var output = document.getElementById('output');
    var cache = {};

    // Если обработка будет работать по событию загрузки страницы
    window.onload = input.onchange = function(event) {
        sync();
    };

    function get(url, cb) {
        // Если сетевой запрос будет закеширован на время работы со страницей.
        if(cache[url]) {
            return cb(null, cache[url]);
        }

        var req = new XMLHttpRequest();

        req.open('GET', url, true);

        req.onreadystatechange = function() {
            if(req.readyState !== 4) return;

            if(req.status === 200) {
                var contentType = req.getResponseHeader('Content-Type') || '';
                contentType = contentType.split(';')[0];

                parse(contentType, req.responseText, function(err, result) {
                    if(err) return cb(err);

                    cache[url] = result;
                    cb(null, result);
                });
            }
            else {
                cb(req.statusText);
            }
        };

        req.send(null);
    }

    function parse(contentType, res, cb) {
        var parser = {
            'application/javascript': executeJs,
            'application/json': parseJson,
            'text/plain': function(res, cb) {
                 cb(null, res);
            }
        }[contentType];

        if(typeof parser === 'function') {
            parser(res, cb);
        }
        else {
            cb('Unknown contentType');
        }
    }

    function executeJs(res, cb) {
        try{
            window.cb = function(data) {
                if(typeof data === 'object' && data.text) {
                    cb(null, data.text);
                }
                else {
                    cb('Text is missing');
                }
                // Если при загрузке js-файла код "уберет за собой"
                delete window.cb;
            };

            (new Function(res))();
        }
        catch(e) {
            cb('Bad javascript');
        }
    }

    function parseJson(res, cb) {
        try {
            var data = JSON.parse(res);

            if(typeof data === 'object' && data.text) {
                cb(null, data.text);
            }
            else {
                cb('Text is missing');
            }
        }
        catch(e) {
            cb('Bad json');
        }
    }

    function sync() {
        // Если предусмотрено отображение слова "Загрузка..." во время загрузки данных.
        output.value = 'Loading...';

        get(input.value, function(err, value) {
            output.value = err ? 'Error: '+ err : value;
        });
    }
})();